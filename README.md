# Advent of Code 2022

Solutions to Advent of Code 2022.

These solutions are likely to be inefficient or take the worse possible approach, so use at your own risk!

I use a custom helper to reduce the effort of setting up a new day.


Set up a new day:

```bash
python new_day.py <day>
```

This will create a directory called `dayx` in `days/`. The directory will have two files: `dayx.py` and `inputx.txt` where `x` is the day number.

There are some utility functions in `utils`, along with the templates for the day directories.

You can run a day with:

```bash
python run_day.py <day>
```

This helper is fine for my purposes, so consider https://github.com/Starwort/aoc_helper for a better option. :)
