from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input6.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput6.txt"


def get_unique_string(string: str, size: int) -> tuple[str, int, int]:
    """Get the first unique substring of the given size with start and end index.

    Args:
        string (str): The string to search
        size (int): The size of unique string to find

    Returns:
        tuple[str, int, int]: The found string, start, and end indexes
    """
    for i in range(len(string)):
        marker = string[i:i+size]
        if len(set(marker)) == size:
            return string[i:i+size], i, i+size-1

    return "", 0, 0


def part_1(buffer: str) -> int:
    return get_unique_string(buffer, 4)[-1] + 1


def part_1_b(buffer: str) -> int:
    marker: list[str] = []

    for count, char in enumerate(buffer, 1):
        if char in marker:
            for _ in range(marker.index(char)+1):
                marker.pop(0)

        marker.append(char)

        if len(marker) == 4:
            return count

    return -1


def part_2(buffer: str) -> int:
    return get_unique_string(buffer, 14)[-1] + 1


def run():
    print(f"Day {DAY}")

    test_data = [line.split() for line in utils.load_input_as_lines(TEST_INPUT_PATH)]

    for buffer, expected_p1, _ in test_data:
        p1_test_result = part_1(buffer)
        p1b_test_result = part_1_b(buffer)
        print("P1 Test:", p1_test_result, p1b_test_result)
        assert p1_test_result == int(expected_p1)

    data = utils.load_input_as_string(INPUT_PATH)

    p1_result = part_1(data)
    print("P1:", p1_result)

    for buffer, _, expected_p2 in test_data:
        p2_test_result = part_2(buffer)
        print("P2 Test:", p2_test_result)
        assert p2_test_result == int(expected_p2)

    p2_result = part_2(data)
    print("P2:", p2_result)
