from pathlib import Path
from time import perf_counter
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input8.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput8.txt"


def part_1(forest: list[list[int]]) -> int:
    visible_trees: set[tuple[int, int]] = set()

    for y, row in enumerate(forest):
        for x, tree in enumerate(row):
            vertical = list(zip(*forest))[x]

            # check if horizontal edge
            if y == 0 or y == len(forest) - 1:
                visible_trees.add((y, x))

            # check if vertical edge
            elif x == 0 or x == len(row) - 1:
                visible_trees.add((y, x))

            # check both sides of current tree for anything taller
            elif max(row[:x]) < tree or max(row[x+1:]) < tree:
                visible_trees.add((y, x))

            # check above and below tree if we're not on an edge
            if 0 < y < len(vertical) - 1:
                if max(vertical[:y]) < tree or max(vertical[y+1:]) < tree:
                    visible_trees.add((y, x))

    return len(visible_trees)


def count_until(value: int, sequence: list[int] | tuple[int, ...]) -> int:
    """Helper function to count the number of elements before reaching a valuer greater
    than or equal to value.

    Args:
        value (int): The value to check
        sequence (list[int] | tuple[int, ...]): The sequence to check

    Returns:
        int: How many elements before reaching a value greater than or equal to value.
             The stop value is also included.
    """
    c = 0
    for x in sequence:
        c += 1
        if x >= value:
            break

    return c


def part_2(forest: list[list[int]]) -> int:
    scenic_scores: list[int] = []
    inverted_forest = list(zip(*forest))

    for y, row in enumerate(forest):
        for x, tree in enumerate(row):
            # check left
            score = count_until(tree, row[:x][::-1])
            # check right
            score *= count_until(tree, row[x+1:])

            vertical = inverted_forest[x]

            # check above
            score *= count_until(tree, vertical[:y][::-1])
            # check below
            score *= count_until(tree, vertical[y+1:])

            scenic_scores.append(score)

    return max(scenic_scores)


def run():
    print(f"Day {DAY}")

    test_data = utils.load_input_as_grid(TEST_INPUT_PATH, func=int)
    data = utils.load_input_as_grid(INPUT_PATH, func=int)

    start = perf_counter()
    p1_test_result = part_1(test_data)
    print("P1 Test:", p1_test_result)
    assert p1_test_result == 21
    print(perf_counter() - start)
    print()

    start = perf_counter()
    p1_result = part_1(data)
    print("P1:", p1_result)
    print(perf_counter() - start)
    print()

    start = perf_counter()
    p2_test_result = part_2(test_data)
    print("P2 Test:", p2_test_result)
    assert p2_test_result == 8
    print(perf_counter() - start)
    print()

    start = perf_counter()
    p2_result = part_2(data)
    print("P2:", p2_result)
    print(perf_counter() - start)
    print()
