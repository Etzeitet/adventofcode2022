from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input3.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput3.txt"


def part_1(data: list[str]):
    priority = 0

    for line in data:
        half = len(line) // 2
        r = (set(line[:half]) & set(line[half:])).pop()
        priority += ord(r) - 96 + (58 * (ord(r) < 97))

    return priority


def part_2(data: list[list[str]]):
    priority = 0

    for group in data:
        r = (set(group[0]) & set(group[1]) & set(group[2])).pop()
        priority += ord(r) - 96 + (58 * (ord(r) < 97))

    return priority


def run():
    print(f"Day {DAY}")

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    p1_test_result = part_1(test_data)
    print("P1 Test:", p1_test_result)
    assert p1_test_result == 157

    data = utils.load_input_as_lines(INPUT_PATH)
    p1_result = part_1(data)
    print("P1:", p1_result)

    test_data = utils.load_input_as_chunks(TEST_INPUT_PATH, chunk_size=3)
    p2_test_result = part_2(test_data)
    print("P2 Test:", p2_test_result)
    assert p2_test_result == 70

    data = utils.load_input_as_chunks(INPUT_PATH, chunk_size=3)
    p2_result = part_2(data)
    print("P2:", p2_result)
