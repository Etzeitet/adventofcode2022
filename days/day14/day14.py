from dataclasses import dataclass
from pathlib import Path
from typing import Any
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input14.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput14.txt"


@dataclass(order=True, frozen=True)
class Point:
    x: int
    y: int


Cave = set[Point]


def parse_cave(rock_paths: list[str]) -> tuple[Cave, int]:
    cave: Cave = set()
    lowest: int = 0

    for line in rock_paths:
        coords_raw = line.split(" -> ")
        coords = [Point(*[int(x) for x in i.split(",")]) for i in coords_raw]

        for p1, p2 in zip(coords, coords[1:]):
            if p1.y > lowest:
                lowest = p1.y

            if p2.y > lowest:
                lowest = p2.y

            if p2 < p1:
                p1, p2 = p2, p1

            for x in range(p1.x, p2.x + 1):
                for y in range(p1.y, p2.y + 1):
                    cave.add(Point(x, y))

    return cave, lowest


def can_move(cave: Cave, point: Point, floor: int, with_floor: bool = False) -> bool:
    return not (point in cave or (with_floor and point.y == floor))


def simulate(cave: Cave, sand_start: Point, lowest: int, with_floor: bool = False) -> int:
    if with_floor:
        floor = lowest + 2

    else:
        floor = lowest

    start = Point(sand_start.x, sand_start.y)
    end = sand_start

    start_cave_len = len(cave)

    sand = start

    while True:
        for dx, dy in (0, 1), (-1, 1), (1, 1):
            next_location = Point(sand.x + dx, sand.y + dy)

            if can_move(cave, next_location, floor, with_floor):
                if next_location.y >= floor:
                    return len(cave) - start_cave_len

                sand = next_location
                break

        else:
            cave.add(sand)

            if with_floor and sand == end:
                break

            sand = start

    return len(cave) - start_cave_len


def part_1(cave: Cave, lowest: int) -> int:
    return simulate(cave, Point(500, 0), lowest)


def part_2(cave: Cave, lowest: int) -> int:
    return simulate(cave, Point(500, 0), lowest, True)


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_cave, test_lowest = parse_cave(utils.load_input_as_lines(TEST_INPUT_PATH))
    cave, lowest = parse_cave(utils.load_input_as_lines(INPUT_PATH))

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_cave.copy(), test_lowest)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 24

    if str(part) == "1" or part is None:
        p1_result = part_1(cave.copy(), lowest)
        output.append(("P1:", p1_result))
        assert p1_result == 674

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_cave.copy(), test_lowest)
        output.append(("P2 Test:", p2_test_result))
        assert p2_test_result == 93

    if str(part) == "2" or part is None:
        p2_result = part_2(cave.copy(), lowest)
        output.append(("P2:", p2_result))
        assert p2_result == 24958

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
