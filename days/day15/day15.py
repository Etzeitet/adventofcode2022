from pathlib import Path
import re
from typing import Any
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input15.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput15.txt"

Point = tuple[int, int]
SensorData = dict[Point, tuple[Point, int]]


def parse_sensor_data(raw_data: list[str]) -> SensorData:
    data: SensorData = {}
    pattern = r"-?\d+"

    for line in raw_data:
        parsed = [int(i.group()) for i in re.finditer(pattern, line)]

        sensor: Point = tuple(parsed[:2])
        beacon: Point = tuple(parsed[2:])

        distance = abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])

        data[sensor] = beacon, distance

    return data


def find_impossible_locations(data: SensorData, line: int) -> tuple[set[Point], set[Point]]:
    devices: set[Point] = set()
    impossible: set[Point] = set()

    for sensor, beacon_data in data.items():
        beacon, manhattan = beacon_data

        devices.add(sensor)
        devices.add(beacon)

        # if line doesn't fall within the distance to a sensor's nearest beacon
        # it has no affect on that line. 0 or more for dist_to_line to "see" line.
        dist_to_line = manhattan - abs(sensor[1] - line)

        if dist_to_line >= 0:
            for x in range(sensor[0] - dist_to_line, sensor[0] + dist_to_line + 1):
                impossible.add((x, line))

    return impossible, devices


def part_1(data: SensorData, line: int) -> int:
    impossible, devices = find_impossible_locations(data, line)
    return len(impossible - devices)


def part_2(data: SensorData, max_line: int) -> int:
    for line in range(max_line + 1):
        ranges: list[tuple[int, int]] = []

        # this loop works out the range of x-coords that the current sensor can "see"
        # based on the distance to its nearest beacon. These ranges are areas that
        # cannot contain our missing beacon
        for sensor, beacon_data in data.items():
            _, manhattan = beacon_data

            dist_to_line = manhattan - abs(sensor[1] - line)

            if dist_to_line >= 0:
                ranges.append((sensor[0]-dist_to_line, sensor[0]+dist_to_line))

        # once we have the ranges, sort them
        ranges.sort()

        # now we have to check if there is any gaps in the ranges. A gap means a sensor
        # could not see that location, which will be where our missing beacon could be
        # lurking.

        end = ranges[0][1]
        for r in ranges[1:]:
            rx1, rx2 = r

            # if start of current range is after the end
            if rx1 - end > 1:
                return ((end+1) * 4000000) + line

            end = max(end, rx2)
    return -1


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_data = parse_sensor_data(utils.load_input_as_lines(TEST_INPUT_PATH))
    data = parse_sensor_data(utils.load_input_as_lines(INPUT_PATH))

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_data, 10)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 26

    if str(part) == "1" or part is None:
        p1_result = part_1(data, 2000000)
        output.append(("P1:", p1_result))
        assert p1_result == 4985193

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_data, 20)
        output.append(("P2 Test:", p2_test_result))
        assert p2_test_result == 56000011

    if str(part) == "2" or part is None:
        p2_result = part_2(data, 4000000)
        output.append(("P2:", p2_result))
        assert True

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
