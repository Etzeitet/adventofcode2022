from pathlib import Path
import days.util.utils as utils

from collections import Counter

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input1.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput1.txt"


def part_1(data: list[list[int]]):
    elves = dict(enumerate(data, 1))

    elf_counts = Counter({k: sum(v) for k, v in elves.items()})
    return sum(e[1] for e in elf_counts.most_common(1))


def part_2(data: list[list[int]]):
    elves = dict(enumerate(data, 1))

    elf_counts = Counter({k: sum(v) for k, v in elves.items()})
    return sum(e[1] for e in elf_counts.most_common(3))


def run():
    print(f"Day {DAY}\n")

    test_input = utils.load_input_as_chunks(TEST_INPUT_PATH, func=int)

    p1_test_result = part_1(test_input)
    print("P1 Test:", p1_test_result)

    data = utils.load_input_as_chunks(INPUT_PATH, func=int)
    p1_result = part_1(data)
    print("P1:", p1_result)
    print()

    print("P2 Test:", part_2(test_input))

    p2_result = part_2(data)
    print("P2:", p2_result)
