from operator import add, sub, mul, truediv
from pathlib import Path
import re
from typing import Any, Callable, Self

import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input11.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput11.txt"


class Monkey:
    def __init__(
        self,
        items: list[int],
        operation: tuple[Callable[[int, int], int], str],
        condition: tuple[int, int, int],
        max_worry: int = 1
    ):
        self._items = items
        self._operation = operation
        self._condition = condition

        self.inspected = 0
        self.max_worry = max_worry

    def _inspect(self, item: int) -> int:
        op, _value = self._operation

        if _value == "old":
            value = item

        else:
            value = int(_value)

        self.inspected += 1

        return op(item, value)

    def _post_inspection(self, item: int, part: int = 1) -> int:
        if part == 1:
            return item // 3

        return item % self.max_worry

    def _get_target(self, item: int) -> int:
        value, true, false = self._condition

        if item % value == 0:
            return true

        return false

    def throw(self, monkeys: list[Self], part: int = 1) -> None:
        item = self._items.pop(0)
        item = self._inspect(item)
        item = self._post_inspection(item, part)

        target_monkey = self._get_target(item)

        monkeys[target_monkey].catch(item)

    def throw_all(self, monkeys: list[Self], part: int = 1) -> None:
        while self._items:
            self.throw(monkeys, part)

    def catch(self, item: int) -> None:
        self._items.append(item)

    def has_items(self) -> bool:
        return bool(self._items)


def parse_monkeys(data: list[str]) -> list[Monkey]:
    ops: dict[str, Callable[[int, int], int]] = {
        "+": add,
        "-": sub,
        "*": mul,
        "/": truediv,
    }

    monkeys: list[Monkey] = []

    i = 0
    max_worry = 1
    while i < len(data):
        items = [int(m.group()) for m in re.finditer(r"\d+", data[i+1])]

        op, value = data[i + 2].split()[-2:]
        operation = (ops[op], value)

        test = int(data[i + 3].rsplit(maxsplit=1)[-1])
        true = int(data[i + 4].rsplit(maxsplit=1)[-1])
        false = int(data[i + 5].rsplit(maxsplit=1)[-1])
        condition = (test, true, false)

        max_worry *= test

        monkey = Monkey(items=items, operation=operation, condition=condition)

        monkeys.append(monkey)
        i += 7

    for monkey in monkeys:
        monkey.max_worry = max_worry
    return monkeys


def part_1(monkey_data: list[str]) -> int:
    monkeys = parse_monkeys(monkey_data)

    for _ in range(20):
        for monkey in monkeys:
            if monkey.has_items():
                monkey.throw_all(monkeys)

    return mul(*sorted(m.inspected for m in monkeys)[-2:])


def part_2(monkey_data: list[str]) -> int:
    monkeys = parse_monkeys(monkey_data)

    for _ in range(10000):
        for monkey in monkeys:
            if monkey.has_items():
                monkey.throw_all(monkeys, 2)

    return mul(*sorted(m.inspected for m in monkeys)[-2:])


def run(part: str | int | None = None, benchmark: bool = False) -> None:

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH, empty_ok=True)
    data = utils.load_input_as_lines(INPUT_PATH, empty_ok=True)

    output: list[Any] = []

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_data)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 10605

    if str(part) == "1" or part is None:
        p1_result = part_1(data)
        output.append(("P1:", p1_result))
        assert p1_result == 88208

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_data)
        output.append(("P2 Test:", p2_test_result))
        assert p2_test_result == 2713310158

    if str(part) == "2" or part is None:
        p2_result = part_2(data)
        output.append(("P2:", p2_result))
        assert p2_result == 21115867968

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
