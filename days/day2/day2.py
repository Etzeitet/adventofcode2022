from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input2.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput2.txt"

SHAPES = {
    "A": 0,
    "B": 1,
    "C": 2,
    "X": 0,
    "Y": 1,
    "Z": 2
}

SCORES = [1, 2, 3]

LOSE_POINTS = 0
DRAW_POINTS = 3
WIN_POINTS = 6


def part_1(data: list[tuple[str, str]]):
    score = 0

    for them_code, us_code in data:
        them = SHAPES[them_code]
        us = SHAPES[us_code]

        if us == them:
            score += DRAW_POINTS

        elif (us + 1) % 3 == them:
            score += LOSE_POINTS

        else:
            score += WIN_POINTS

        score += SCORES[us]

    return score


def get_shape(code: str, them: int):
    shapes = [0, 1, 2]

    if code == "X":
        shape = shapes[them - 1]

    if code == "Y":
        shape = them

    if code == "Z":
        i = (them + 1) % 3
        shape = shapes[i]

    return shape


def part_2(data: list[tuple[str, str]]):
    score = 0

    for them_code, us_code in data:
        them = SHAPES[them_code]
        us = get_shape(us_code, them)

        if us == them:
            score += DRAW_POINTS

        elif (us + 1) % 3 == them:
            score += LOSE_POINTS

        else:
            score += WIN_POINTS

        score += us + 1

    return score


def run():
    print(f"Day {DAY}")

    test_data = [line.split() for line in utils.load_input_as_lines(TEST_INPUT_PATH)]

    p1_test_result = part_1(test_data)
    print("P1 Test:", p1_test_result)
    assert p1_test_result == 15

    data = [line.split() for line in utils.load_input_as_lines(INPUT_PATH)]
    p1_result = part_1(data)
    print("P1:", p1_result)

    p2_test_result = part_2(test_data)
    print("P2 Test:", p2_test_result)
    assert p2_test_result == 12

    p2_result = part_2(data)
    print("P2:", p2_result)
