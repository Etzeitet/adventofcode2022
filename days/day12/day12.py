import heapq
from pathlib import Path
from typing import Any
import days.util.utils as utils

Point = tuple[int, int]
Terrain = list[str]

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input12.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput12.txt"


def in_bounds(terrain: Terrain, x: int, y: int) -> bool:
    max_x = len(terrain[0]) - 1
    max_y = len(terrain) - 1

    return 0 <= x <= max_x and 0 <= y <= max_y


def traverse_terrain(terrain: Terrain, source: Point, dest: Point, asc: bool = True) -> int:
    """Traverse a terrain.

    Can either go from low elevation to high (asc is True, default) or from
    high elevation to low (asc is False).

    Parameters
    ----------
    terrain : Terrain
        The terrain to traverse
    source : Point
        Source point
    dest : Point
        destination point, ignored if asc = False
    asc : bool, optional
        ascending (low to high) or not, by default True

    Returns
    -------
    int
        Number of steps to reach the destination.
    """
    visited: list[Point] = [source]
    heap: list[tuple[int, int, int]] = [(0,) + source]

    destination = dest if asc else "a"

    while heap:
        steps, y, x = heapq.heappop(heap)

        if (y, x) == destination or terrain[y][x] == destination:
            return steps

        for ox, oy in ((0, -1), (1, 0), (0, 1), (-1, 0)):
            dx = x + ox
            dy = y + oy

            if (dy, dx) in visited or not in_bounds(terrain, dx, dy):
                continue

            current_elevation = ord(terrain[y][x]) - 97
            next_elevation = ord(terrain[dy][dx]) - 97
            delta_elevation = next_elevation - current_elevation

            if not asc:
                delta_elevation *= -1

            if delta_elevation <= 1:
                heapq.heappush(heap, (steps + 1, dy, dx))
                visited.append((dy, dx))

    return 0


def part_1(terrain: Terrain) -> int:
    source = (int(), int())
    dest = (int(), int())

    terrain = terrain.copy()

    # get the start and dest points and convert
    # S and E values to their elevation equivalents
    for y in range(len(terrain)):
        for x in range(len(terrain[0])):
            if terrain[y][x] == "S":
                source = (y, x)
                terrain[y] = terrain[y].replace("S", "a")

            if terrain[y][x] == "E":
                dest = (y, x)
                terrain[y] = terrain[y].replace("E", "z")

    steps = traverse_terrain(terrain, source, dest)

    return steps


def part_2(terrain: Terrain) -> int:
    source = (int(), int())

    terrain = terrain.copy()

    # get the start point and convert E value to z
    for y in range(len(terrain)):
        for x in range(len(terrain[0])):
            if terrain[y][x] == "E":
                source = (y, x)
                terrain[y] = terrain[y].replace("E", "z")

    steps = traverse_terrain(terrain, source, (-1, -1), asc=False)

    return steps


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    data = utils.load_input_as_lines(INPUT_PATH)

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_data)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 31

    if str(part) == "1" or part is None:
        p1_result = part_1(data)
        output.append(("P1:", p1_result))
        assert p1_result == 520

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_data)
        output.append(("P2 Test:", p2_test_result))
        assert p2_test_result == 29

    if str(part) == "2" or part is None:
        p2_result = part_2(data)
        output.append(("P2:", p2_result))
        assert p2_result == 508

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
