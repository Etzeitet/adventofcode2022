from pathlib import Path
from typing import Any
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input7.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput7.txt"

FS = dict[str, "int | FS"]


def parse_transcript(transcript: list[str]) -> FS:
    """Parse command transcript into filesystem object (dict)

    Args:
        transcript (list[str]): The command transcript

    Returns:
        FS: A dict representing the filesystem
    """
    filesystem: FS = {"/": {}}

    previous_dir: list[FS] = []

    # dumb hack to keep pylance/pyright happy
    fs = filesystem["/"]
    assert not isinstance(fs, int)
    current_dir: FS = fs

    for line in transcript[1:]:
        parts = line.split()

        if line == "$ cd ..":
            current_dir = previous_dir.pop()

        elif parts[1] == "cd":
            assert not isinstance(current_dir, int)
            previous_dir.append(current_dir)

            new_dir = current_dir[parts[2]]
            assert not isinstance(new_dir, int)
            current_dir = new_dir

        elif parts[0] == "dir":
            current_dir.setdefault(parts[1], {})

        elif parts[0].isdigit():
            current_dir[parts[1]] = int(parts[0])

    return filesystem


def get_directory_size(
    filesystem: FS, dirs: dict[str, int] | None = None
) -> tuple[int, dict[str, int]]:
    """Return the size of the given filesystem and also sizes of intermediate directories
    traversed.

    Args:
        filesystem (FS): A filesystem dict
        dirs (dict | None, optional): A dict to hold the individual directory sizes.
                                      Defaults to None.

    Returns:
        tuple[int, dict[str, int]]: The total size and intermediate sizes
    """
    if dirs is None:
        dirs = {}

    size = 0
    for name, value in filesystem.items():
        if isinstance(value, int):
            size += value

        if isinstance(value, dict):
            size_, dirs = get_directory_size(value, dirs)

            # deal with duplicate directory names
            suffix = 0
            while name in dirs:
                suffix += 1
                name = f"{name}{suffix}"

            dirs[name] = size_
            size += size_

    return size, dirs


def part_1(filesystem: FS) -> int:
    _, dir_sizes = get_directory_size(filesystem)

    return sum(size for size in dir_sizes.values() if size <= 100_000)


def part_2(filesystem: FS) -> int:
    _, dir_sizes = get_directory_size(filesystem)
    max_size = 70_000_000
    required = 30_000_000
    remaining = max_size - dir_sizes["/"]

    return min(size for size in dir_sizes.values() if remaining + size >= required)


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    test_filesystem = parse_transcript(test_data)

    data = utils.load_input_as_lines(INPUT_PATH)
    filesystem = parse_transcript(data)

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_filesystem)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 95437

    if str(part) == "1" or part is None:
        p1_result = part_1(filesystem)
        output.append(("P1:", p1_result))

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_filesystem)
        output.append(("P2 Test", p2_test_result))
        assert p2_test_result == 24933642

    if str(part) == "2" or part is None:
        p2_result = part_2(filesystem)
        output.append(("P2:", p2_result))

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
