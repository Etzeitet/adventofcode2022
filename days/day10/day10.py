from pathlib import Path
from typing import Generator
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input10.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput10.txt"


def run_cpu(instructions: list[str]) -> Generator[tuple[int, int], None, None]:
    cycle = 1
    x = 1

    for instruction in instructions:
        if instruction == "noop":
            increment = 0
            cycles = 1

        else:
            increment = int(instruction.split()[-1])
            cycles = 2

        for _ in range(cycles):
            yield cycle, x
            cycle += 1

        x += increment


def part_1(instructions: list[str]) -> int:
    return sum(
        [
            cycle * x
            for cycle, x in run_cpu(instructions)
            if cycle >= 20 and cycle % 40 == 20
        ][:6]
    )


def part_2(instructions: list[str]) -> str:
    crt = ""

    for cycle, x in run_cpu(instructions):
        crt_pos = (cycle - 1) % 40
        pixel = "#" if crt_pos in (x-1, x, x+1) else "."
        crt += f"{pixel}\n" if crt_pos == 39 else pixel

    return crt


def run(part: str | int | None = None) -> None:
    print(f"Day {DAY}")

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    data = utils.load_input_as_lines(INPUT_PATH)

    if str(part) == "1_test" or part is None:
        p1_test_result1 = part_1(
            ["noop", "addx 3", "addx -5"]
        )
        print("P1 Test:", p1_test_result1)
        assert True

    if str(part) == "1_test1" or part is None:
        p1_test_result1 = part_1(test_data)
        print("P1 Test:", p1_test_result1)
        assert p1_test_result1 == 13140

    if str(part) == "1" or part is None:
        p1_result = part_1(data)
        print("P1:", p1_result)
        assert True

    if str(part) == "2_test" or part is None:
        expected_result = """##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
"""

        p2_test_result = part_2(test_data)
        print("P2 Test:\n\n", p2_test_result, "\n", sep="")
        assert p2_test_result == expected_result

    if str(part) == "2" or part is None:
        expected_result = """###....##.####.###..#..#.###..####.#..#.
#..#....#.#....#..#.#..#.#..#.#....#..#.
###.....#.###..#..#.####.#..#.###..#..#.
#..#....#.#....###..#..#.###..#....#..#.
#..#.#..#.#....#.#..#..#.#.#..#....#..#.
###...##..#....#..#.#..#.#..#.#.....##..
"""

        p2_result = part_2(data)
        print("P2:\n\n", p2_result, "\n", sep="")
        assert p2_result == expected_result
