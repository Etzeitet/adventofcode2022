from pathlib import Path
from typing import Any, Callable, TypeVar

from more_itertools import chunked, split_at

R = TypeVar("R")


def load_input_as_string(path: Path) -> str:
    """Load the input file as a single string.

    Args:
        path (Path): Path to the input file

    Returns:
        str: Contents of the input file as a single string
    """
    with path.open("r") as f:
        data = f.read()

    return data


def load_one_line_as_list(
    path: Path, delim: str = ",", as_int: bool = False, as_float: bool = False
) -> list[Any]:
    """Load a single line from the input file into a list using delim.

    Individual list items can be further converted to int or float.

    Args:
        path (Path): Path to the input file
        delim (str, optional): Delimiter to split line on. Defaults to ",".
        as_int (bool, optional): Convert list items to int. Defaults to False.
        as_float (bool, optional): Convert list items to float. Defaults to False.

    Returns:
        list[Any]: List of items
    """
    line = load_input_as_string(path).split(delim)

    func: Callable[[str], Any] = str

    if as_int:
        func = int

    elif as_float:
        func = float

    return [func(x) for x in line]


def load_input_as_lines(
    path: Path,
    as_int: bool = False,
    as_float: bool = False,
    func: Callable[[str], R] = str,
    empty_ok: bool = False,
    skip_empty: bool = False,
) -> list[R]:
    """Load the input file as a list of lines.

    Indivdual list items can be converts to an int or float. Empty lines can be
    skipped or converted to None.

    Args:
        path (Path): Path to the input file.
        as_int (bool, optional): Convert lines to int. Defaults to False.
        as_float (bool, optional): Convert lines to float. Defaults to False.
        empty_ok (bool, optional): If True will not raise error on empty lines.
        Defaults to False.
        empty_as_none (bool, optional): if empty_ok, choose to convert empty lines to None.
        Defaults to True.
        skip_empty (bool, optional): If True, ignores empty lines. Defaults to False.

    Raises:
        ValueError: If empty_ok is False and an empty line is encountered.

    Returns:
        list[Any]: The list of items.
    """
    if as_int or as_float:
        raise ValueError(
            "as_int and as_float are deprecated. Supply a function to func parameter instead."
        )

    with path.open("r") as f:
        data = f.read().splitlines()

    lines: list[R] = []

    for line in data:
        if not line:
            if not empty_ok and not skip_empty:
                raise ValueError("Empty line found in input.")

            if skip_empty:
                continue

        lines.append(func(line))

    return lines


def load_input_as_chunks(
    path: Path, func: Callable[[str], R] = str, chunk_size: int = -1, delim: str = ""
) -> list[list[R]]:
    """Load input as a list of chunks. The last chunk may be shorter than the rest.

    Args:
        path (Path): Path to the input file.
        func (Callable[[str], R], optional): Function to convert each element to. Defaults to str.
        chunk_size (int, optional): Size of chunk. Defaults to -1.
        delim (str, optional): How to split the chunks if chunk_size not set. Defaults to "".

    Returns:
        list[list[R]]: The list of chunks
    """
    with path.open("r") as f:
        data = f.read().splitlines()

    if chunk_size > 0:
        chunks = chunked(data, chunk_size)

    else:
        chunks = split_at(data, lambda x: x == delim)

    return [[func(x) for x in chunk] for chunk in chunks]


def load_input_as_grid(path: Path, func: Callable[[Any], R] = str) -> list[list[R]]:
    with path.open("r") as f:
        data = f.read().splitlines()

    return [[func(x) for x in row] for row in data]
