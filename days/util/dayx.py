from pathlib import Path
from typing import Any
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / ":INPUT_PATH:"
TEST_INPUT_PATH = Path(__file__).parent / ":TEST_INPUT_PATH:"


def part_1(x: Any) -> Any:
    print("Not implemented")


def part_2(x: Any) -> Any:
    print("Not implemented")


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    data = utils.load_input_as_lines(INPUT_PATH)

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_data)
        output.append(("P1 Test:", p1_test_result))
        assert True

    if str(part) == "1" or part is None:
        p1_result = part_1(data)
        output.append(("P1:", p1_result))
        assert True

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_data)
        output.append(("P2 Test:", p2_test_result))
        assert True

    if str(part) == "2" or part is None:
        p2_result = part_2(data)
        output.append(("P2:", p2_result))
        assert True

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
