from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input4.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput4.txt"


def part_1(data: list[str]) -> int:
    fully_contained_ranges = 0

    for line in data:
        a, b, c, d = map(int, line.replace(",", "-").split("-"))

        if (c >= a and d <= b) or (a >= c and b <= d):
            fully_contained_ranges += 1

    return fully_contained_ranges


def part_2(data: list[str]) -> int:
    overlapping_ranges = 0

    for line in data:
        a, b, c, d = map(int, line.replace(",", "-").split("-"))

        if (c <= a <= d) or (a <= c <= b) or (c <= b <= d) or (a <= d <= b):
            overlapping_ranges += 1

    return overlapping_ranges


def run():
    print(f"Day {DAY}")

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    p1_test_result = part_1(test_data)
    print("P1 Test:", p1_test_result)
    assert p1_test_result == 3

    data = utils.load_input_as_lines(INPUT_PATH)
    p1_result = part_1(data)
    print("P1:", p1_result)

    p2_test_result = part_2(test_data)
    print("P2 Test:", p2_test_result)
    assert p2_test_result == 5

    p2_result = part_2(data)
    print("P2:", p2_result)
