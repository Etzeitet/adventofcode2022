from pathlib import Path
from typing import Self

import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input9.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput9.txt"
TEST_INPUT_PATH_2 = Path(__file__).parent / "testinput9_2.txt"


class Knot:
    def __init__(self, x: int, y: int, knot: Self | None = None, num: int = 0) -> None:
        self.x = x
        self.y = y
        self.knot = knot
        self.num = num

        self.visited = {(self.x, self.y)}

        self.directions = {
            "U": (0, 1),
            "D": (0, -1),
            "L": (-1, 0),
            "R": (1, 0)
        }

    def follow(self, h_x: int, h_y: int) -> None:
        delta_x = abs(self.x - h_x)
        delta_y = abs(self.y - h_y)

        if delta_x > 1 and self.y == h_y:
            self.x += 1 if h_x > self.x else -1

        elif delta_y > 1 and self.x == h_x:
            self.y += 1 if h_y > self.y else -1

        elif delta_x > 1 or delta_y > 1:
            self.x += 1 if h_x > self.x else -1
            self.y += 1 if h_y > self.y else -1

        self.visited.add((self.x, self.y))

        if self.knot:
            self.knot.follow(self.x, self.y)

    def move(self, direction: str, distance: int) -> None:

        delta = self.directions[direction]

        for _ in range(distance):
            self.x += delta[0]
            self.y += delta[1]

            self.visited.add((self.x, self.y))

            if self.knot:
                self.knot.follow(self.x, self.y)

    def unique_visited(self, nknot: int = 0) -> int:
        """Get the count of unique locations visited.

        If knot is specified, the visited locations for the nth knot will be
        returned instead.
        """
        k = self

        for _ in range(nknot):
            if k.knot is None:
                break

            k = k.knot

        return len(k.visited)


def build_rope(knots: int) -> Knot:
    """Build a rope consisting of at least one Head.

    knots is the number of knots, or tails, to add to the head.
    """
    head = Knot(0, 0)
    last_rope = head

    for i in range(knots):
        last_rope.knot = Knot(0, 0, num=i+1)
        last_rope = last_rope.knot

    return head


def run_instructions(instructions: list[str], rope: Knot) -> None:
    for instruction in instructions:
        direction, distance = instruction.split()
        distance = int(distance)

        rope.move(direction, distance)


def part_1(instructions: list[str], knots: int) -> int:
    head = build_rope(knots)
    run_instructions(instructions, head)

    return head.unique_visited(knots)


def part_2(instructions: list[str], knots: int) -> int:
    head = build_rope(knots)
    run_instructions(instructions, head)

    return head.unique_visited(knots)


def run(part: str | int | None = None) -> None:
    print(f"Day {DAY}")

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH)
    data = utils.load_input_as_lines(INPUT_PATH)
    test_data2 = utils.load_input_as_lines(TEST_INPUT_PATH_2)

    if str(part) in ("1_test", "1_test1") or part is None:
        p1_test_result = part_1(instructions=test_data, knots=1)
        print("P1 Test:", p1_test_result)
        assert p1_test_result == 13

    if str(part) == "1" or part is None:
        p1_result = part_1(instructions=data, knots=1)
        print("P1:", p1_result)
        assert p1_result == 6271

    if str(part) in ("2_test", "2_test2") or part is None:
        p2_test_result = part_2(instructions=test_data, knots=9)
        print("P2 Test 1:", p2_test_result)
        assert p2_test_result == 1

    if str(part) == "2_test2" or part is None:
        p2_test_result2 = part_2(instructions=test_data2, knots=9)
        print("P2 Test 2:", p2_test_result2)
        assert p2_test_result2 == 36

    if str(part) == "2" or part is None:
        p2_result = part_2(instructions=data, knots=9)
        print("P2:", p2_result)
        assert p2_result == 2458
