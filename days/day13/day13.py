from ast import literal_eval
from functools import cmp_to_key
from itertools import zip_longest
from pathlib import Path
from typing import Any
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input13.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput13.txt"

Packet = list["int | Packet"]


def compare_packets(p1: Packet, p2: Packet) -> int:
    if p1 and not p2:
        return 1

    if p2 and not p1:
        return -1

    for e1, e2 in zip_longest(p1, p2):
        if e1 is None:
            return -1

        if e2 is None:
            return 1

        if isinstance(e1, int) and isinstance(e2, int):
            if e1 < e2:
                return -1

            elif e2 < e1:
                return 1

        else:

            e1 = e1 if isinstance(e1, list) else [e1]
            e2 = e2 if isinstance(e2, list) else [e2]

            r = compare_packets(e1, e2)
            if r != 0:
                return r

    return 0


def part_1(packets: list[Packet]) -> int:
    index = 1
    total = 0

    for p1, p2 in zip(packets[::2], packets[1::2]):
        if compare_packets(p1, p2) == -1:
            total += index

        index += 1

    return total


def part_2(packets: list[Packet]) -> int:
    packets.extend(
        [[[2]], [[6]]]
    )
    sorted_packets = sorted(packets, key=cmp_to_key(compare_packets))

    i1 = sorted_packets.index([[2]]) + 1
    i2 = sorted_packets.index([[6]]) + 1

    return i1 * i2


def run(part: str | int | None = None, benchmark: bool = False) -> None:
    output: list[Any] = []

    test_data: list[Packet] = utils.load_input_as_lines(
        TEST_INPUT_PATH, skip_empty=True, func=literal_eval
    )

    data: list[Packet] = utils.load_input_as_lines(INPUT_PATH, skip_empty=True, func=literal_eval)

    if str(part) == "1_test" or part is None:
        p1_test_result = part_1(test_data)
        output.append(("P1 Test:", p1_test_result))
        assert p1_test_result == 13

    if str(part) == "1" or part is None:
        p1_result = part_1(data)
        output.append(("P1:", p1_result))
        assert p1_result == 6272

    if str(part) == "2_test" or part is None:
        p2_test_result = part_2(test_data)
        output.append(("P2 Test:", p2_test_result))
        assert p2_test_result == 140

    if str(part) == "2" or part is None:
        p2_result = part_2(data)
        output.append(("P2:", p2_result))
        assert p2_result == 22288

    if not benchmark:
        print(f"Day {DAY}")

        for line in output:
            print(*line)
