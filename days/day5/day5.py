from itertools import zip_longest
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input5.txt"
TEST_INPUT_PATH = Path(__file__).parent / "testinput5.txt"


def parse_crates(lines: list[str]) -> list[list[str]]:
    max_col = max(map(int, lines[-1].replace(" ", "")))
    crates: list[list[str]] = [[] for _ in range(max_col)]

    for line in lines[:-1]:
        for i, chunk in enumerate(zip_longest(*[iter(line)] * 4)):
            if chunk[1].isalpha():
                crates[i].insert(0, chunk[1])

    return crates


def parse_moves(lines: list[str]) -> list[list[int]]:
    moves = []
    for line in lines:
        _, count, _, from_, _, to_ = line.split()
        moves.append([int(x) for x in (count, from_, to_)])

    return moves


def part_1(moves: list[list[int]], crates: list[list[str]]) -> str:
    for count, from_, to_ in moves:
        col = crates[from_-1]
        remainder = col[0:-count]
        moving = col[-1:-count-1:-1]
        crates[to_-1].extend(moving)
        crates[from_-1] = remainder

    result = "".join(col[-1] for col in crates)

    return result


def part_2(moves: list[list[int]], crates: list[list[str]]) -> str:
    for count, from_, to_ in moves:
        col = crates[from_-1]
        remainder = col[0:-count]
        moving = col[-count:]
        crates[to_-1].extend(moving)
        crates[from_-1] = remainder

    result = "".join(col[-1] for col in crates)

    return result


def run():
    print(f"Day {DAY}")

    test_data = utils.load_input_as_lines(TEST_INPUT_PATH, empty_ok=True)
    test_crates = parse_crates(test_data[:4])
    test_moves = parse_moves(test_data[5:])

    p1_test_result = part_1(test_moves, test_crates)
    print("P1 Test:", p1_test_result)
    assert p1_test_result == "CMZ"

    data = utils.load_input_as_lines(INPUT_PATH, empty_ok=True)
    s_index = data.index(None)

    moves = parse_moves(data[s_index+1:])
    crates = parse_crates(data[:s_index])

    p1_result = part_1(moves, crates)
    print("P1:", p1_result)

    test_crates = parse_crates(test_data[:4])
    test_moves = parse_moves(test_data[5:])

    p2_test_result = part_2(test_moves, test_crates)
    print("P2 Test:", p2_test_result)
    assert p2_test_result == "MCD"

    moves = parse_moves(data[s_index+1:])
    crates = parse_crates(data[:s_index])

    p2_result = part_2(moves, crates)
    print("P2:", p2_result)
