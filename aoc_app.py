from textual.app import App, ComposeResult
from textual.containers import Container, Vertical
from textual.widgets import Button, Footer, Header, Placeholder, Static


HOME_TEXT = """\
# Advent of Code 2022

Please select a day either by clicking the corresponding item on the left or
using your number keys.
"""

DAY_1_TEXT = """\
As you finally start making your way upriver, you realize your pack is much lighter than you remember. Just then, one of the items from your pack goes flying overhead. Monkeys are playing Keep Away with your missing things!

To get your stuff back, you need to be able to predict where the monkeys will throw your items. After some careful observation, you realize the monkeys operate based on how worried you are about each item.

You take some notes (your puzzle input) on the items each monkey currently has, how worried you are about those items, and how the monkey makes decisions based on your worry level. For example:
"""

DAY_2_TEXT = """\
The preparations are finally complete; you and the Elves leave camp on foot and begin to make your way toward the star fruit grove.

As you move through the dense undergrowth, one of the Elves gives you a handheld device. He says that it has many fancy features, but the most important one to set up right now is the communication system.

However, because he's heard you have significant experience dealing with signal-based systems, he convinced the other Elves that it would be okay to give you their one malfunctioning device - surely you'll have no problem fixing it.

As if inspired by comedic timing, the device emits a few colorful sparks.

To be able to communicate with the Elves, the device needs to lock on to their signal. The signal is a series of seemingly-random characters that the device receives one at a time.
"""


class AOC(App):
    CSS_PATH = "aoc.css"

    def compose(self) -> ComposeResult:
        yield Header(show_clock=True, name="Advent of Code")
        yield Container(
            Vertical(
                Static("Sidebar"),
                *[Button(f"Day {i}") for i in range(10)],
                id="sidebar"
            ),
            Static(HOME_TEXT, id="body")
        )
        yield Footer()


if __name__ == "__main__":
    app = AOC()
    app.run()  # type: ignore
