import argparse
import sys
from pathlib import Path


def create_dir(directory: Path):
    if directory.exists():
        raise ValueError("Directory exists")

    directory.mkdir()


def create_day(day: int, directory: Path) -> Path:
    day_dir = directory / f"day{day}"

    try:
        day_dir.mkdir()

    except FileExistsError:
        print(f"Day {day} already exists.")
        sys.exit()

    day_path = day_dir / f"day{day}.py"
    input_path = day_dir / f"input{day}.txt"
    test_input_path = day_dir / f"testinput{day}.txt"
    init_path = day_dir / "__init__.py"
    template = Path("days/util/dayx.py")

    with open(template) as t, open(day_path, "w") as d:
        for line in t:
            if line.startswith("INPUT_PATH"):
                line = line.replace(":INPUT_PATH:", input_path.name)

            if line.startswith("TEST_INPUT_PATH"):
                line = line.replace(":TEST_INPUT_PATH:", test_input_path.name)

            d.write(line)

    input_path.touch(exist_ok=True)
    test_input_path.touch(exist_ok=True)
    init_path.touch(exist_ok=True)

    return day_dir


def main(config: argparse.Namespace):
    day_dir = create_day(config.day, Path(__file__).parent / "days")

    print(f"Created {day_dir} for day {config.day}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("day", type=int, help="day number.")
    parser.add_argument("-i", help="puzzle input.")

    config = parser.parse_args()

    main(config)
